//Gaurab Dahal (L20432055)

package com.gaurabdahal.servicetest1;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

public class MainService extends MyIntentService {
    boolean is_running, is_playing, is_paused, ui_onscreen;
    public  static final String ACTION_PLAY = "com.gaurabdahal.servicetest1.PLAY";
    public  static final String ACTION_STOP = "com.gaurabdahal.servicetest1.STOP";
    public static final String ACTION_PAUSE = "com.gaurabdahal.servicetest1.PAUSE";
    public static final String ACTION_UI_ONSCREEN = "com.gaurabdahal.servicetest1.UI_ONSCREEN";
    public static final String ACTION_UI_OFFSCREEN = "com.gaurabdahal.servicetest1.UI_OFFSCREEN";

    MediaPlayer mp;
    int progress = 0;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public MainService() {
        super("MyService");
        is_running = false;
        is_playing = false;
        is_paused = false;
    }


    // The service is being created
    @Override
    public void onCreate() {
        // do any start initializing here
        super.onCreate();

        ui_onscreen=true;
        //initialise the media player
        mp=null;
        mp = MediaPlayer.create(this, R.raw.wordplay);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                is_running = false;
                is_playing=false;
                if(!ui_onscreen) {
                    notifyMe();
                }
            }
        });

    }

    // Called when the service is no longer used and is being destroyed
    @Override
    public void onDestroy() {
        super.onDestroy();
        is_running = false;
    }

    // Not using but needed so stub this out (return null)
    @Override
    public IBinder onBind(Intent intent) { return null; }

    /**
     * This method is invoked on the worker thread with a request to process.
     * Only one Intent is processed at a time, but the processing happens on a
     * worker thread that runs independently from other application logic.
     * So, if this code takes a long time, it will hold up other requests to
     * the same IntentService, but it will not hold up anything else.
     * When all requests have been handled, the IntentService stops itself,
     * so you should not call {@link #stopSelf}.
     *
     * @param intent The value passed to {@link
     *               Context#startService(Intent)}.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if(action.equals(ACTION_PLAY)){
            ui_onscreen=true;
            if (! is_running) {
               //TODO start playing media
               // resetPlayer();
                if(is_playing)
                    mp.reset();
                mp.start();
                is_running = true;
                is_playing = true;
            }
        }else if(action.equals(ACTION_STOP)) {
            ui_onscreen=true;
            is_playing = false;
            is_paused = false;
            is_running = false;
            if(mp != null){
                //stop playing audio and then set mp to null
                mp.release();
                mp=null;
            }
            stopSelf();
        }else if(action.equals(ACTION_PAUSE)) {
            ui_onscreen=true;
           //TODO pause the audio
            if(is_playing){
                progress = mp.getCurrentPosition();
                mp.pause();
                is_paused=true;
                is_playing=false;
            }else if(is_paused){
            //TODO or unpause the audio if paused
                mp.seekTo(progress);
                mp.start();
                is_paused=false;
                is_playing = true;
            }


        }else if(action.equals(ACTION_UI_ONSCREEN)) {
            ui_onscreen=true;
        }else if(action.equals(ACTION_UI_OFFSCREEN)) {
            ui_onscreen=false;
        }

    }

    public void notifyMe(){
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,0,resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentTitle("Gaurab Dahal")
                .setContentText("Audio has ended");

        //sets an ID for the notification
        int mNotificationId = 999;
        // gets and instance of the notification manager service
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //Builds the notificationa nd issues it
        manager.notify(mNotificationId,mBuilder.build());
    }


}
