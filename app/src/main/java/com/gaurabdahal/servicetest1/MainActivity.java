//Gaurab Dahal (L20432055)
package com.gaurabdahal.servicetest1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button buttonPlay, buttonStop, buttonPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonPlay = (Button) findViewById(R.id.buttonStart);
        buttonStop    = (Button) findViewById(R.id.buttonStop);
        buttonPause = (Button) findViewById(R.id.buttonMessage);
        buttonPlay.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
        buttonPause.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonPlay) {
            Log.i("gaurab", "started");
            Intent intent = new Intent(this, MainService.class);
            intent.setAction(MainService.ACTION_PLAY);
            startService(intent)  ;
        }
        else if (v == buttonStop) {
            //stopService(new Intent(getBaseContext(), MainService.class));
            Intent intent = new Intent(this, MainService.class);
            intent.setAction(MainService.ACTION_STOP);
            startService(intent)  ;
        }
        else if (v == buttonPause) {
           /* Intent intent = new Intent(getBaseContext(), MainService.class);
            intent.putExtra("action", "send me a message");
            startService(intent);*/
            Intent intent = new Intent(this, MainService.class);
            intent.setAction(MainService.ACTION_PAUSE);
            startService(intent)  ;
        }
    }

    @Override
    protected void onResume(){
        Intent intent = new Intent(this, MainService.class);
        intent.setAction(MainService.ACTION_UI_ONSCREEN);
        startService(intent)  ;
        super.onResume();
    }

    @Override
    protected void onPause(){
        Intent intent = new Intent(this, MainService.class);
        intent.setAction(MainService.ACTION_UI_OFFSCREEN);
        startService(intent)  ;
        super.onPause();
    }
}